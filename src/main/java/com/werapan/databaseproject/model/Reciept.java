/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.werapan.databaseproject.model;

import com.werapan.databaseproject.dao.CustomerDao;
import com.werapan.databaseproject.dao.UserDao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import com.werapan.databaseproject.model.RecieptDetail;

/**
 *
 * @author werapan
 */
public class Reciept {

    private int id;
    private Date created_date;
    private float total;
    private float cash;
    private int total_qty;
    private int user_id;
    private int customer_id;
    private User user;
    private Customer customer;
    private ArrayList<RecieptDetail> recieptDetails = new ArrayList();

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getCreated_date() {
        return created_date;
    }

    public void setCreated_date(Date created_date) {
        this.created_date = created_date;
    }

    public float getTotal() {
        return total;
    }

    public void setTotal(float total) {
        this.total = total;
    }

    public float getCash() {
        return cash;
    }

    public void setCash(float cash) {
        this.cash = cash;
    }

    public int getTotal_qty() {
        return total_qty;
    }

    public void setTotal_qty(int total_qty) {
        this.total_qty = total_qty;
    }

    public int getUserId() {
        return user_id;
    }

    public void setUserId(int user_id) {
        this.user_id = user_id;
    }

    public int getCustomerId() {
        return customer_id;
    }

    public void setCustomerId(int customer_id) {
        this.customer_id = customer_id;
    }

    public Reciept(int id, Date created_date, float total, float cash, int total_qty, int user_id, int customer_id) {
        this.id = id;
        this.created_date = created_date;
        this.total = total;
        this.cash = cash;
        this.total_qty = total_qty;
        this.user_id = user_id;
        this.customer_id = customer_id;
    }

    public Reciept(float cash, int user_id, int customer_id) {
        this.id = -1;
        this.created_date = null;
        this.total = 0;
        this.cash = cash;
        this.total_qty = 0;
        this.user_id = user_id;
        this.customer_id = customer_id;
    }

    public Reciept() {
        this.id = -1;
        this.created_date = null;
        this.total = 0;
        this.cash = 0;
        this.total_qty = 0;
        this.user_id = 0;
        this.customer_id = 0;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
        this.user_id = user.getId();
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
        this.customer_id = customer.getId();
    }

    public ArrayList<RecieptDetail> getRecieptDetails() {
        return recieptDetails;
    }

    public void setRecieptDetails(ArrayList recieptDetails) {
        this.recieptDetails = recieptDetails;
    }

    @Override
    public String toString() {
        return "Reciept{" + "id=" + id + ", created_date=" + created_date + ", total=" + total + ", cash=" + cash + ", total_qty=" + total_qty + ", user_id=" + user_id + ", customer_id=" + customer_id + ", user=" + user + ", customer=" + customer + ", recieptDetails=" + recieptDetails + '}';
    }

    public void addRecieptDetail(RecieptDetail recieptDetail) {
        recieptDetails.add(recieptDetail);
        calculateTotal();
    }

    public void addRecieptDetail(Product product, int qty) {
        RecieptDetail rd = new RecieptDetail(product.getId(), product.getName(), product.getPrice(), qty, qty * product.getPrice(), -1);
        recieptDetails.add(rd);
        calculateTotal();
    }

    public void delRecieptDetail(RecieptDetail recieptDetail) {
        recieptDetails.remove(recieptDetail);
        calculateTotal();
    }

    public void calculateTotal() {
        int totalQty = 0;
        float total = 0.0f;
        for (RecieptDetail rd : recieptDetails) {
            total += rd.getTotalPrice();
            totalQty += rd.getQty();
        }
        this.total_qty = totalQty;
        this.total = total;

    }

    public static Reciept fromRS(ResultSet rs) {
        Reciept reciept = new Reciept();
        try {
            reciept.setId(rs.getInt("reciept_id"));
            reciept.setCreated_date(rs.getTimestamp("reciept_id"));
            reciept.setTotal(rs.getFloat("total"));
            reciept.setCash(rs.getFloat("cash"));
            reciept.setTotal(rs.getFloat("total_qty"));
            reciept.setUserId(rs.getInt("user_id"));
            reciept.setCustomerId(rs.getInt("customer_id"));

            CustomerDao customerDao = new CustomerDao();
            Customer customer = customerDao.get(reciept.getCustomerId());
            UserDao userDao = new UserDao();
            User user = userDao.get(reciept.getUserId());
            reciept.setCustomer(customer);
            reciept.setUser(user);
        } catch (SQLException ex) {
            Logger.getLogger(Reciept.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return reciept;
    }
}
